CPP=clang++
CPPFLAGS=$(DEBUG) -Wall -fPIC -std=c++17 

PACKS=tech2019.o tech2020.o

all: devpack

.depend:
	$(CPP) -E -MM *.cpp > .depend

devpack: main.cpp $(PACKS)
	$(CPP) $(CPPFLAGS) -v main.cpp $(PACKS) -o devpack

clean:
	rm -rf *.o *~ devpack

.SUFFIXES: .cpp .o
.cpp.o:
	$(CPP) $(CPPFLAGS) -c $<


