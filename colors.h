#pragma once

// raw colors
#define NORMAL    "\e[0m"
#define BLACK     "\e[0;30m"
#define RED       "\e[0;31m"
#define GREEN     "\e[0;32m"
#define YELLOW    "\e[0;33m"
#define BLUE      "\e[0;34m"
#define MAGENTA   "\e[0;35m"
#define CYAN      "\e[0;36m"
#define WHITE     "\e[0;37m"
#define BRBLACK   "\e[0;90m"
#define BRRED     "\e[0;91m"
#define BRGREEN   "\e[0;92m"
#define BRYELLOW  "\e[0;93m"
#define BRBLUE    "\e[0;94m"
#define BRMAGENTA "\e[0;95m"
#define BRCYAN    "\e[0;96m"
#define BRWHITE   "\e[0;97m"

// text colors
#define CLR_DEF WHITE     // default
#define CLR_LBL BRBLACK   // label
#define CLR_UNI WHITE     // unique
#define CLR_TXT BRWHITE   // text
#define CLR_ERR RED       // error
#define CLR_WRN YELLOW    // warning
#define CLR_SHP BRCYAN    // ship
#define CLR_PLT BRBLUE    // pilot
#define CLR_UPT GREEN     // upgrade type
#define CLR_UPG BRGREEN   // upgrade
#define CLR_UNR RED       // unreleased
#define CLR_KEY BRWHITE   // settings key
#define CLR_VAL WHITE     // settigns value
// stat colors
#define CLR_INI YELLOW    // pilot skill
#define CLR_ATK BRRED     // attack
#define CLR_AGI BRGREEN   // agility
#define CLR_HUL BRYELLOW  // hull
#define CLR_SHD BRCYAN    // shield
#define CLR_FRC BRMAGENTA // force
#define CLR_CHG YELLOW    // charge
#define CLR_CST BRWHITE   // cost
#define CLR_RNG BRWHITE   // range
#define CLR_ENR BRMAGENTA // energy
// faction colors
#define CLR_REB RED       // rebel faction
#define CLR_IMP GREEN     // imperial faction
#define CLR_SCU YELLOW    // scum faction
#define CLR_RES YELLOW    // resistance faction
#define CLR_FIR BRBLACK   // first order faction
#define CLR_REP RED       // republic faction
#define CLR_SEP CYAN      // separatist faction


#define CLR_DIF RED
#define CLR_FLV BRBLACK
#define CLR_BSP BRWHITE"\u001b[44m"
#define CLR_404 BRWHITE"\u001b[41m"
#define CLR_BOW BLACK"\u001b[47m"
#define CLR_BCK BRGREEN
#define CLR_DEV BRBLACK
#define CLR_CLO BRBLACK
#define CLR_DEC BRWHITE
