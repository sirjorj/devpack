#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <vector>

#include <sys/ioctl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "tech2019.h"
#include "tech2020.h"

// Linux Mint Terminal
// Monospace Regular (28)
// Set antialiasing to grayscale


using namespace std;



/*
Quick thoughts on the actions we haven’t defined yet:
shield `( )`
hull `[ ]`
front arc `\/`
Back arc `/\`
Right arc `<`
Left arc `>`
Full front arc and full back arc - not sure. We need this for moralo! Maybe `_o_` and `-o-`
Single turret arc `(^)` or `o^`
Double turret arc `(v^)` or `vo^` or `v^`
agility `->`

slam `>>`
reload `^v`
focus `<o>`
target lock `[+]`
barrel roll `@`
boost `\^/`
evade `~>`
jam `-w-`
reinforce `{>)`
coordinate `v^v` or `^A^`
Rotate arc `(^)` or `^>` (edited)
*/

std::string StripAnsi(const std::string src) {
  std::string ret = regex_replace(src, std::regex("\e\[[0-9;]*m"), "");
  return ret;
}

int main(int argc, char* argv[]) {

  int rows, cols;
  int padding=1;
  {
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    rows = w.ws_row;
    cols = w.ws_col;
  }

  std::vector<std::stringstream> cards;

#if 0
  cards.push_back(tech2019::BlueScreenPilot());
  cards.push_back(tech2019::CountDown());
  cards.push_back(tech2019::RegexBrath());
  cards.push_back(tech2019::EvalMoralo());
  cards.push_back(tech2019::ElloWorld());
  cards.push_back(tech2019::Null());
  cards.push_back(tech2019::Back());
#endif

#if 1
  cards.push_back(tech2020::Back());
  cards.push_back(tech2020::BashRendar());
  cards.push_back(tech2020::Echo());
  cards.push_back(tech2020::SataPortHunter());
  cards.push_back(tech2020::CobolSqBomber());
  cards.push_back(tech2020::MajVonNeumann());
  cards.push_back(tech2020::MaceWindows());
  cards.push_back(tech2020::DBS404());
  cards.push_back(tech2020::BafflingElectronics());
  cards.push_back(tech2020::ForkBomb());
#endif

  std::vector<int> perLine;
  {
    int curPos=0;
    int count=0;
    for(std::stringstream& ss : cards) {
      int space = curPos ? padding : 0;
      int endline = 2;
      std::string line;
      getline(ss, line);
      ss.seekg(0);
      int newLen = StripAnsi(line).length();
      //printf("is %d + %d + %d + %d (%d) > %d\n", curPos, space, newLen, endline, curPos + space + newLen + endline, cols);
      if(curPos + space + newLen + endline > cols) {
	//printf("  yes\n");
	perLine.push_back(count);
	curPos = space + newLen;
	count = 1;
      } else {
	//printf(" no\n");
	curPos += space + newLen;
	count++;
      }
    }
    perLine.push_back(count);
  }

  int offset = 0;
  for(int pl : perLine) {
    bool remaining = true;
    while(remaining) {
      remaining = false;
      for(int i=offset; i<pl+offset; i++) {
	std::string line;
	if(cards[i].good()) { remaining = true; }
	getline(cards[i], line);
	if(i != offset) {
	  for(int s=0; s<padding; s++) {
	    cout << "  ";
	  }
	}
	cout << line;
      }
      cout << endl;
    }
    offset += pl;
  }

  #if 0
  if(argc > 1 && std::string(argv[1]) == "h") {
    // print them horizontally
    while(cards[0].good()) {
      for(std::stringstream& ss : cards) {
	std::string line;
	getline(ss, line);
	cout << line;
	cout << "  ";
      }
      cout << endl;
    }
  } else {
    // print them vertically
    cout << endl << endl;
    for(std::stringstream& ss : cards) {
      std::string line;
      while(ss.good()) {
	getline(ss, line);
	cout << "    " << line << endl;
      }
    }
  }
  #endif

  return 0;
}
