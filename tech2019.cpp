#include "tech2019.h"

namespace tech2019 {

using namespace std;

std::stringstream Back() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|"<<CLR_DEV<<"ajmath andrelind  angryalbatross"<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"antigrapist   chibi   chrisallen"<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"chrisbrown    danb   dennis.bale"<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"derrkater     devjohnny      dlo"<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"earthworm  ekesicki  eli.stevens"<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"fab74 ferg  firstearth freakydug"<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_CST<<"         xwvl.slack.com         "<<CLR_DEF<<"|" << endl;
  ss << "|" <<CLR_BCK<<"      __ __  __            __   "<<CLR_DEF<<"|" << endl;
  ss << "|" <<CLR_BCK<<"   __/ // /_/ /____  _____/ /_  "<<CLR_DEF<<"|" << endl;
  ss << "|" <<CLR_BCK<<"  /_  _  __/ __/ _ \\/ ___/ __ \\ "<<CLR_DEF<<"|" << endl;
  ss << "|" <<CLR_BCK<<" /_  _  __/ /_/  __/ /__/ / / / "<<CLR_DEF<<"|" << endl;
  ss << "|" <<CLR_BCK<<"  /_//_/  \\__/\\___/\\___/_/ /_/  "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"gadwag    geordan   guidokessels"<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"haslo  infinityloop jamesdowdall"<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"jesper   jimbob   jonaspeltomaki"<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"jychuah  kdubb kelrien  kerbocat"<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"klutz    lyynark    majorjuggler"<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"mrmurphm  mu0n  mwhited  raithos"<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"sirjorj  so_crates   sozin  stiv"<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"terter thelogicdump vanderlegion"<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"          whittaker007          "<<CLR_DEF<<"|" << endl;
  ss << "+--------------------------------+" << endl;
  return ss;
}





std::stringstream BlueScreenPilot() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|"<<CLR_BSP<<"                                "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_BSP<<"                 8              "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_BSP<<"                 |              "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_BSP<<"      m----------+<=>( )        "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_BSP<<"                 |              "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_BSP<<"                 8              "<<CLR_DEF<<"|" << endl;
  ss << "|--------------------------------|" << endl;
  ss << "| " << CLR_INI << "2" << CLR_DEF << "  " << CLR_TXT << "Blue Screen Pilot" << CLR_DEF << "       " << CLR_REB << "REB" << CLR_DEF << " |" << endl;
  ss << "|                                |" << endl;
  ss << "| "<<CLR_FLV<<"The B-wing has heavy"<<CLR_DEF<<"  "<<CLR_TXT<<"<@>"<<CLR_DEF<<" > "<<CLR_DIF<<"_o_"<<CLR_DEF<<"|" << endl;
  ss << "| "<<CLR_FLV<<"weapons and shields."<<CLR_DEF<<"           |" << endl;
  ss << "| "<<CLR_FLV<<"It is an innovative"<<CLR_DEF<<"      "<<CLR_TXT<<"[+]"<<CLR_DEF<<"   |" << endl;
  ss << "| "<<CLR_FLV<<"assault fighter."<<CLR_DEF<<"               |" << endl;
  ss << "|                          "<<CLR_TXT<<"_o_"<<CLR_DEF<<"   |" << endl;
  ss << "|                                |" << endl;
  ss << "|                                |" << endl;
  ss << "|                                |" << endl;
  ss << "|                                |" << endl;
  ss << "|  "<<CLR_ATK<<"\\ /"<<CLR_DEF<<"  "<<CLR_AGI<<"-->"<<CLR_DEF<<"  "<<CLR_HUL<<"[ ]"<<CLR_DEF<<"  "<<CLR_SHD<<"( )"<<CLR_DEF<<"            |" << endl;
  ss << "|   "<<CLR_ATK<<"3"<<CLR_DEF<<"    "<<CLR_AGI<<"1"<<CLR_DEF<<"    "<<CLR_HUL<<"4"<<CLR_DEF<<"    "<<CLR_SHD<<"4"<<CLR_DEF<<"             |" << endl;
  ss << "|         A/SF-01 B-wing         |" << endl;
  ss << "+--------------------------------+" << endl;
  return ss;
}

std::stringstream CountDown() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|                                |" << endl;
  ss << "|                                |" << endl;
  ss << "|     _,-=@=-._______________    |"  << endl;
  ss << "|     \\ \"/K\"        __,.--\"\"     |" << endl;
  ss << "|      \\/__\\,..--\"\"              |" << endl;
  ss << "|                                |" << endl;
  ss << "|--------------------------------|" << endl;
  ss << "| " << CLR_INI << "4" << CLR_DEF << "         " << CLR_TXT << "*\"Count--\"" << CLR_DEF << "       " << CLR_IMP << "IMP" << CLR_DEF << " |" << endl;
  ss << "|          "<<CLR_FLV<<"Death Defier"<<CLR_DEF<<"          |" << endl;
  ss << "| While you defend, after    "<<CLR_TXT<<"<@>"<<CLR_DEF<<" |" << endl;
  ss << "| the Neutralize Results         |" << endl;
  ss << "| step, if you are not       "<<CLR_TXT<<"~~>"<<CLR_DEF<<" |" << endl;
  ss << "| stressed, you may suffer       |" << endl;
  ss << "| 1 {HIT} damage and gain 1  "<<CLR_TXT<<"_o_"<<CLR_DEF<<" |" << endl;
  ss << "| stress token.  If you do,      |" << endl;
  ss << "| cancel all dice results.       |" << endl;
  ss << "|                                |" << endl;
  ss << "|       "<<CLR_FLV<<"Adaptive Ailerons"<<CLR_DEF<<"        |" << endl;
  ss << "|     "<<CLR_ATK<<"\\ /"<<CLR_DEF<<"    "<<CLR_AGI<<"-->"<<CLR_DEF<<"    "<<CLR_HUL<<"[ ]"<<CLR_DEF<<"          |" << endl;
  ss << "|      "<<CLR_ATK<<"3"<<CLR_DEF<<"      "<<CLR_AGI<<"2"<<CLR_DEF<<"      "<<CLR_HUL<<"4"<<CLR_DEF<<"           |" << endl;
  ss << "|         TIE/sk Striker         |" << endl;
  ss << "+--------------------------------+" << endl;
  return ss;
}

std::stringstream RegexBrath() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|" << RED << "##" << BRRED << "####" << YELLOW << "###" << BRRED << "#" << YELLOW << "#" << BRRED << "##" << RED << "#" << BRRED << "#" << BRBLACK << "\\__  \\___\\" << BRRED << "#" << RED << "#" << BRBLACK << "##" << RED << "#" << BRRED << "#" << YELLOW << "#" <<CLR_DEF<<"|" << endl;
  ss << "|" << BRBLACK << "###" << RED << "##" << BRRED << "####" << YELLOW << "##" << BRYELLOW << "#" << YELLOW << "###" << BRYELLOW << "##" << YELLOW << "#" << BRBLACK << "/|" << YELLOW << "##" << BRRED << "#" << RED << "##" << BRBLACK << "###" << RED << "#" << BRRED << "#" << RED << "#" << BRBLACK << "#" <<CLR_DEF<<"|" << endl;
  ss << "|" << BRBLACK << "########" << RED << "#" << BRRED << "#" << YELLOW << "##" << BRYELLOW << "####" << BRBLACK << "/ " << BRWHITE << "/ \\" << BRBLACK << "\\" << YELLOW << "#" << BRRED << "##" << RED << "###" << BRBLACK << "#" << BRBLACK << "###" <<CLR_DEF<<"|" << endl;
  ss << "|" << RED << "#" << BRRED << "#" << RED << "##" << BRBLACK << "###" << RED << "##" << BRRED << "##" << BRBLACK << "\\_   _L" << BRWHITE << "\\_/" << BRBLACK << "./  __\\" << BRRED << "#" << RED << "#" << BRBLACK << "##" <<CLR_DEF<<"|" << endl;
  ss << "|" << BRBLACK << "#####" << RED << "#" << BRRED << "##" << YELLOW << "#" << BRYELLOW << "##" << YELLOW << "##" << BRBLACK << "7  \\" << YELLOW << "#" << BRYELLOW << "##" << BRBLACK << "\\   7" << BRYELLOW << "#" << YELLOW << "#" << BRRED << "#" << RED << "#" << BRBLACK << "###" <<CLR_DEF<<"|" << endl;
  ss << "|" << BRBLACK << "##" << RED << "##" << BRRED << "##" << YELLOW << "##" << BRYELLOW << "#" << YELLOW << "#" << BRRED << "##" << YELLOW << "#" << BRBLACK << "\\__ \\" << BRYELLOW << "#" << YELLOW << "#" << BRRED << "#" << RED << "#" << BRBLACK << "\\_  \\" << BRYELLOW << "##" << YELLOW << "##" << BRRED << "#" <<CLR_DEF<<"|" << endl;
  ss << "|--------------------------------|" << endl;
  ss << "| " << CLR_INI << "5" << CLR_DEF << "       " << CLR_TXT << "*Regex Brath" << CLR_DEF << "       " << CLR_IMP << "IMP" << CLR_DEF << " |" << endl;
  ss << "|          "<<CLR_FLV<<"Onyx Leader"<<CLR_DEF<<"           |" << endl;
  ss << "| After you perform an       "<<CLR_TXT<<"<@>"<<CLR_DEF<<" |" << endl;
  ss << "| attack that hits, if           |" << endl;
  ss << "| you are evading, expose    "<<CLR_TXT<<"~~>"<<CLR_DEF<<" |" << endl;
  ss << "| 1 of the defender's            |" << endl;
  ss << "| damage cards.              "<<CLR_TXT<<"[+]"<<CLR_DEF<<" |" << endl;
  ss << "|                                |" << endl;
  ss << "|                            "<<CLR_TXT<<"_o_"<<CLR_DEF<<" |" << endl;
  ss << "|                                |" << endl;
  ss << "|       "<<CLR_FLV<<"Full Throttle"<<CLR_DEF<<"        "<<CLR_TXT<<"\\|/"<<CLR_DEF<<" |" << endl;
  ss << "|  "<<CLR_ATK<<"\\ /"<<CLR_DEF<<"  "<<CLR_AGI<<"-->"<<CLR_DEF<<"  "<<CLR_HUL<<"[ ]"<<CLR_DEF<<"  "<<CLR_SHD<<"( )"<<CLR_DEF<<"            |" << endl;
  ss << "|   "<<CLR_ATK<<"3"<<CLR_DEF<<"    "<<CLR_AGI<<"3"<<CLR_DEF<<"    "<<CLR_HUL<<"3"<<CLR_DEF<<"    "<<CLR_SHD<<"4"<<CLR_DEF<<"             |" << endl;
  ss << "|         TIE/D Defender         |" << endl;
  ss << "+--------------------------------+" << endl;
  return ss;
}

std::stringstream EvalMoralo() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|                                |" << endl;
  ss << "|" << YELLOW << "                -----------===  " << CLR_DEF << "|" << endl;
  ss << "|" << YELLOW << "        __" << BRBLACK << "U" << YELLOW <<"_   /" << RED << "---" << YELLOW << "         /   " << CLR_DEF << "|" << endl;
  ss << "|" << YELLOW << "  _____/   /_/             /    " << CLR_DEF << "|" << endl;
  ss << "|" << YELLOW <<"  \\---/__/------------" << RED << "====" << YELLOW << "/     " << CLR_DEF << "|" << endl;
  ss << "|                                |" << endl;
  ss << "|--------------------------------|" << endl;
  ss << "| " << CLR_INI << "4" << CLR_DEF << "      " << CLR_TXT << "*Eval(Moralo)" << CLR_DEF << "       " << CLR_SCU << "SCM" << CLR_DEF << " |" << endl;
  ss << "|      "<<CLR_FLV<<"Criminal Mastermind"<<CLR_DEF<<"       |" << endl;
  ss << "| If you would flee, you     "<<CLR_TXT<<"<@>"<<CLR_DEF<<" |" << endl;
  ss << "| may spend 1 (Z).  If you       |" << endl;
  ss << "| do, place yourself in      "<<CLR_TXT<<"{>)"<<CLR_DEF<<" |" << endl;
  ss << "| reserves instead.  At the      |" << endl;
  ss << "| start of the next Planning "<<CLR_TXT<<"[+]"<<CLR_DEF<<" |" << endl;
  ss << "| Phase, place yourself within   |" << endl;
  ss << "| range 1 of the edge of the     |" << endl;
  ss << "| play area that you fled from.  |" << endl;
  ss << "|                                |" << endl;
  ss << "|  "<<CLR_ATK<<"-o-"<<CLR_DEF<<"  "<<CLR_AGI<<"-->"<<CLR_DEF<<"  "<<CLR_HUL<<"[ ]"<<CLR_DEF<<"  "<<CLR_SHD<<"( )"<<CLR_DEF<<"  "<<CLR_CHG<<"(Z)"<<CLR_DEF<<"       |" << endl;
  ss << "|   "<<CLR_ATK<<"3"<<CLR_DEF<<"    "<<CLR_AGI<<"1"<<CLR_DEF<<"    "<<CLR_HUL<<"9"<<CLR_DEF<<"    "<<CLR_SHD<<"3"<<CLR_DEF<<"    "<<CLR_CHG<<"2"<<CLR_DEF<<"        |" << endl;
  ss << "|     YV-666 Light Freighter     |" << endl;
  ss << "+--------------------------------+" << endl;
  return ss;
}

std::stringstream ElloWorld() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|                                |" << endl;
  ss << "|   " << BRWHITE << "o__" << WHITE << "     __     __     " << BRWHITE << "__o    " << CLR_DEF << "|" << endl;
  ss << "|      " << BRBLUE << "\"\"" << BRWHITE << "--" << WHITE << "'--`" << BRWHITE << "/^\\" << WHITE << "'--`" << BRWHITE << "--" << BRBLUE << "\"\"       " << CLR_DEF << "|" << endl;
  ss << "|        " << BRWHITE << "__" << WHITE << ",--." << BRWHITE << "\\V/" << WHITE << ",--." << BRWHITE << "__         " << CLR_DEF << "|" << endl;
  ss << "|   " << BRWHITE << "o--" << BRBLUE << "\"\"   " << WHITE << "\"\"     \"\"   " << BRBLUE << "\"\"" << BRWHITE << "--o    " << CLR_DEF << "|" << endl;
  ss << "|                                |" << endl;
  ss << "|--------------------------------|" << endl;
  ss << "| " << CLR_INI << "5" << CLR_DEF << "        " << CLR_TXT << "*Ello World" << CLR_DEF << "       " << CLR_RES << "RES" << CLR_DEF << " |" << endl;
  ss << "|          "<<CLR_FLV<<"Born to Ill"<<CLR_DEF<<"           |" << endl;
  ss << "| After you reveal a red     "<<CLR_TXT<<"<@>"<<CLR_DEF<<" |" << endl;
  ss << "| Tallon Roll maneuver,          |" << endl;
  ss << "| if you have 2 or fewer     "<<CLR_TXT<<"[+]"<<CLR_DEF<<" |" << endl;
  ss << "| stress tokens, treat           |" << endl;
  ss << "| that maneuver as white.    "<<CLR_TXT<<"\\|/"<<CLR_DEF<<" |" << endl;
  ss << "|                                |" << endl;
  ss << "|                                |" << endl;
  ss << "|                                |" << endl;
  ss << "|                                |" << endl;
  ss << "|  "<<CLR_ATK<<"\\ /"<<CLR_DEF<<"  "<<CLR_AGI<<"-->"<<CLR_DEF<<"  "<<CLR_HUL<<"[ ]"<<CLR_DEF<<"  "<<CLR_SHD<<"( )"<<CLR_DEF<<"            |" << endl;
  ss << "|   "<<CLR_ATK<<"3"<<CLR_DEF<<"    "<<CLR_AGI<<"2"<<CLR_DEF<<"    "<<CLR_HUL<<"4"<<CLR_DEF<<"    "<<CLR_SHD<<"3"<<CLR_DEF<<"             |" << endl;
  ss << "|          T-70 X-wing           |" << endl;
  ss << "+--------------------------------+" << endl;
  return ss;
}

std::stringstream Null() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|                                |" << endl;
  ss << "|                                |" << endl;
  ss << "|            | ,_  |             |" << endl;
  ss << "|            |=(_)=|             |" << endl;
  ss << "|            |     |             |" << endl;
  ss << "|                                |" << endl;
  ss << "|--------------------------------|" << endl;
  ss << "| " << CLR_INI << "0" << CLR_DEF << "           " << CLR_TXT << "*null" << CLR_DEF << "          " << CLR_FIR << "F O" << CLR_DEF << " |" << endl;
  ss << "|          "<<CLR_FLV<<"Epsilon Ace"<<CLR_DEF<<"           |" << endl;
  ss << "| While you are not          "<<CLR_TXT<<"<@>"<<CLR_DEF<<" |" << endl;
  ss << "| damaged, treat your            |" << endl;
  ss << "| initiative value as 7.     "<<CLR_TXT<<"~~>"<<CLR_DEF<<" |" << endl;
  ss << "|                                |" << endl;
  ss << "|                            "<<CLR_TXT<<"[+]"<<CLR_DEF<<" |" << endl;
  ss << "|                                |" << endl;
  ss << "|                            "<<CLR_TXT<<"_o_"<<CLR_DEF<<" |" << endl;
  ss << "|                                |" << endl;
  ss << "|                                |" << endl;
  ss << "|  "<<CLR_ATK<<"\\ /"<<CLR_DEF<<"  "<<CLR_AGI<<"-->"<<CLR_DEF<<"  "<<CLR_HUL<<"[ ]"<<CLR_DEF<<"  "<<CLR_SHD<<"( )"<<CLR_DEF<<"            |" << endl;
  ss << "|   "<<CLR_ATK<<"2"<<CLR_DEF<<"    "<<CLR_AGI<<"3"<<CLR_DEF<<"    "<<CLR_HUL<<"3"<<CLR_DEF<<"    "<<CLR_SHD<<"1"<<CLR_DEF<<"             |" << endl;
  ss << "|         TIE/fo Fighter         |" << endl;
  ss << "+--------------------------------+" << endl;
  return ss;
}

}
