#pragma once
#include "colors.h"
#include <sstream>


namespace tech2019 {
  std::stringstream Back();
  std::stringstream BlueScreenPilot();
  std::stringstream CountDown();
  std::stringstream RegexBrath();
  std::stringstream EvalMoralo();
  std::stringstream ElloWorld();
  std::stringstream Null();
}
