#include "tech2020.h"

namespace tech2020 {

using namespace std;

std::stringstream Back() {

  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|"<<CLR_DEV<<"   André Lind      antigrapist  "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"            Chris Allen         "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<" dennis.bale                    "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"                 eli.stevens    "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"        gadwag                  "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"                                "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_CST<<"         xwvl.slack.com         "<<CLR_DEF<<"|" << endl;
  ss << "|" <<CLR_BCK<<"      __ __   2020         __   "<<CLR_DEF<<"|" << endl;
  ss << "|" <<CLR_BCK<<"   __/ // /_/ /____  _____/ /_  "<<CLR_DEF<<"|" << endl;
  ss << "|" <<CLR_BCK<<"  /_  _  __/ __/ _ \\/ ___/ __ \\ "<<CLR_DEF<<"|" << endl;
  ss << "|" <<CLR_BCK<<" /_  _  __/ /_/  __/ /__/ / / / "<<CLR_DEF<<"|" << endl;
  ss << "|" <<CLR_BCK<<"  /_//_/  \\__/\\___/\\___/_/ /_/  "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"                                "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"                                "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"       guidokessels     jychuah "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<" Kerbocat      Kieran S.        "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"                          muOn  "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"   protoskull        raithos    "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"              sirjorj           "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"    so_crates                   "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_DEV<<"               thelogicdump     "<<CLR_DEF<<"|" << endl;
  ss << "+--------------------------------+" << endl;
  return ss;
}

std::stringstream BashRendar() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|       ,-'````''--,,       __   |" << endl;
  ss << "|    _/     U U      ``--,,||||  |" << endl;
  ss << "|   |       I_I     _______   |  |" << endl;
  ss << "|   |_      [_]    |_______} {   |" << endl;
  ss << "|     \\    "<<BRBLACK<<"O O O"<<CLR_DEF<<"  _________   |  |" << endl;
  ss << "|       \\________|         "<<BRBLACK<<"|__|"<<CLR_DEF<<"  |" << endl;
  ss << "|--------------------------------|" << endl;
  ss << "| " << CLR_INI << "5" << CLR_DEF << "     " << CLR_TXT << "*/bin/bash Rendar" << CLR_DEF << "    " << CLR_REB << "REB" << CLR_DEF << " |" << endl;
  ss << "|       "<<CLR_FLV<<"Hotshot Mercenary"<<CLR_DEF<<"        |" << endl;
  ss << "| While you move,            "<<CLR_TXT<<"<@>"<<CLR_DEF<<" |" << endl;
  ss << "| you ignore obstacles.          |" << endl;
  ss << "| "<<CLR_FLV<<"Sensor Blindspot:"<<CLR_DEF<<"          "<<CLR_TXT<<"[+]"<<CLR_DEF<<" |" << endl;
  ss << "| While you perform a            |" << endl;
  ss << "| primary attack at attack   "<<CLR_DIF<<"_o_"<<CLR_DEF<<" |" << endl;
  ss << "| range 0-1, do not apply        |" << endl;
  ss << "| the range 0-1 bonus        "<<CLR_TXT<<"(^)"<<CLR_DEF<<" |" << endl;
  ss << "| and roll 1 fewer attack        |" << endl;
  ss << "| die.                           |" << endl;
  ss << "|   "<<CLR_ATK<<"<o>"<<CLR_DEF<<"   "<<CLR_AGI<<"-->"<<CLR_DEF<<"   "<<CLR_HUL<<"[ ]"<<CLR_DEF<<"   "<<CLR_SHD<<"( )"<<CLR_DEF<<"        |" << endl;
  ss << "|    "<<CLR_ATK<<"4"<<CLR_DEF<<"     "<<CLR_AGI<<"2"<<CLR_DEF<<"     "<<CLR_HUL<<"6"<<CLR_DEF<<"     "<<CLR_SHD<<"4"<<CLR_DEF<<"         |" << endl;
  ss << "|    YT-2400 Light Freighter     |" << endl;
  ss << "+--------------------------------+" << endl;
  return ss;
}

std::stringstream Echo() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|     "<<CLR_DEC<<"@echo on"<<CLR_DEF<<"       "<<CLR_CLO<<"@echo off"<<CLR_DEF<<"   |" << endl;
  ss << "|      "<<CLR_DEC<<"/ | \\"<<CLR_DEF<<"          "<<CLR_CLO<<"/ | \\"<<CLR_DEF<<"      |" << endl;
  ss << "|     "<<CLR_DEC<<"/] | [\\"<<CLR_DEF<<"        "<<CLR_CLO<<"/] | [\\"<<CLR_DEF<<"     |"  << endl;
  ss << "|    "<<CLR_DEC<<"/ ] | [ \\"<<CLR_DEF<<"      "<<CLR_CLO<<"/ ]_|_[ \\"<<CLR_DEF<<"    |" << endl;
  ss << "|    "<<CLR_DEC<<"[_]{H}[_]"<<CLR_DEF<<"      "<<CLR_CLO<<"[_]{H}[_]"<<CLR_DEF<<"    |" << endl;
  ss << "|      "<<CLR_DEC<<"'\"^\"'"<<CLR_DEF<<"          "<<CLR_CLO<<"'\"^\"'"<<CLR_DEF<<"      |" << endl;
  ss << "|--------------------------------|" << endl;
  ss << "| " << CLR_INI << "4" << CLR_DEF << "         " << CLR_TXT << "*\"echo\"" << CLR_DEF << "          " << CLR_IMP << "IMP" << CLR_DEF << " |" << endl;
  ss << "|       "<<CLR_FLV<<"Slippery Trickster"<<CLR_DEF<<"       |" << endl;
  ss << "| While you decloak, you     "<<CLR_TXT<<"<@>"<<CLR_DEF<<" |" << endl;
  ss << "| must use the [2↖] or [2↗]      |" << endl;
  ss << "| template instead of the    "<<CLR_TXT<<"~~>"<<CLR_DEF<<" |" << endl;
  ss << "| [2↑] template.                 |" << endl;
  ss << "|                            "<<CLR_TXT<<"_o_"<<CLR_DEF<<" |" << endl;
  ss << "|        "<<CLR_FLV<<"Stygium Array"<<CLR_DEF<<"           |" << endl;
  ss << "|                            "<<CLR_TXT<<"(7)"<<CLR_DEF<<" |" << endl;
  ss << "|                                |" << endl;
  ss << "|                                |" << endl;
  ss << "|   "<<CLR_ATK<<"\\ /"<<CLR_DEF<<"   "<<CLR_AGI<<"-->"<<CLR_DEF<<"   "<<CLR_HUL<<"[ ]"<<CLR_DEF<<"   "<<CLR_SHD<<"( )"<<CLR_DEF<<"        |" << endl;
  ss << "|    "<<CLR_ATK<<"3"<<CLR_DEF<<"     "<<CLR_AGI<<"2"<<CLR_DEF<<"     "<<CLR_HUL<<"3"<<CLR_DEF<<"     "<<CLR_SHD<<"2"<<CLR_DEF<<"         |" << endl;
  ss << "|         TIE/ph Phantom         |" << endl;
  ss << "+--------------------------------+" << endl;

  return ss;
}

std::stringstream SataPortHunter() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|          "<<BRBLACK<<"+-----------"<<CLR_DEF<<"          |" << endl;
  ss << "|          "<<BRBLACK<<"|"<<CLR_DEF<<"                     |" << endl;
  ss << "|                "<<BRBLACK<<"_"<<CLR_DEF<<"               |" << endl;
  ss << "|  "<<BRBLACK<<"_"<<CLR_DEF<<"    _"<<BRBLACK<<"______"<<CLR_DEF<<"_"<<BRBLACK<<"poq"<<BRBLACK<<"__"<<BRBLACK<<"____"<<CLR_DEF<<"_    "<<BRBLACK<<"_"<<CLR_DEF<<"  |" << endl;
  ss << "| "<<BRBLACK<<"(_p"<<CLR_DEF<<"==/_____"<<BRYELLOW<<"_"<<BRBLACK<<"o"<<CLR_DEF<<"_/_\\_"<<BRBLACK<<"o"<<BRYELLOW<<"_____"<<CLR_DEF<<"\\=="<<BRBLACK<<"q_)"<<CLR_DEF<<" |" << endl;
  ss << "|       \\______|___|_____/       |" << endl;
  ss << "|--------------------------------|" << endl;
  ss << "| " << CLR_INI << "2" << CLR_DEF << "     " << CLR_TXT << "SATA Port Hunter" << CLR_DEF << "     " << CLR_SCU << "SCM" << CLR_DEF << " |" << endl;
  ss << "|                                |" << endl;
  ss << "| "<<CLR_FLV<<"Crime syndicates augment"<<CLR_DEF<<"   "<<CLR_TXT<<"<@>"<<CLR_DEF<<" |" << endl;
  ss << "| "<<CLR_FLV<<"the lethal skills of"<<CLR_DEF<<"           |" << endl;
  ss << "| "<<CLR_FLV<<"their loyal contractors"<<CLR_DEF<<"    "<<CLR_TXT<<"~~>"<<CLR_DEF<<" |" << endl;
  ss << "| "<<CLR_FLV<<"with the best technology"<<CLR_DEF<<"       |" << endl;
  ss << "| "<<CLR_FLV<<"available, like the fast"<<CLR_DEF<<"   "<<CLR_TXT<<"[+]"<<CLR_DEF<<" |" << endl;
  ss << "| "<<CLR_FLV<<"and formidable Lancer-"<<CLR_DEF<<"         |" << endl;
  ss << "| "<<CLR_FLV<<"class pursuit craft."<<CLR_DEF<<"       "<<CLR_TXT<<"(^)"<<CLR_DEF<<" |" << endl;
  ss << "|                                |" << endl;
  ss << "|                                |" << endl;
  ss << "|   "<<CLR_ATK<<"\\ /"<<CLR_DEF<<"  "<<CLR_ATK<<" o>"<<CLR_DEF<<"  "<<CLR_AGI<<"-->"<<CLR_DEF<<"  "<<CLR_HUL<<"[ ]"<<CLR_DEF<<"  "<<CLR_SHD<<"( )"<<CLR_DEF<<"      |" << endl;
  ss << "|    "<<CLR_ATK<<"3"<<CLR_DEF<<"    "<<CLR_ATK<<"2"<<CLR_DEF<<"    "<<CLR_AGI<<"2"<<CLR_DEF<<"    "<<CLR_HUL<<"8"<<CLR_DEF<<"    "<<CLR_SHD<<"2"<<CLR_DEF<<"       |" << endl;
  ss << "|   Lancer-class Pursuit Craft   |" << endl;
  ss << "+--------------------------------+" << endl;
  return ss;
}

std::stringstream CobolSqBomber() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|     b"<<RED<<"-------______"<<CLR_DEF<<"     "<<BRBLACK<<"_"<<CLR_DEF<<"       |" << endl;
  ss << "|   ([______________====D__      |" << endl;
  ss << "|   "<<BRBLACK<<"--"<<CLR_DEF<<"]'--"<<BRBLACK<<"---F="<<CLR_DEF<<"                  |" << endl;
  ss << "|     |    "<<RED<<"|"<<CLR_DEF<<"                     |" << endl;
  ss << "|     |   "<<BRBLACK<<"/"<<CLR_DEF<<"                      |" << endl;
  ss << "|     {__o--                     |" << endl;
  ss << "|--------------------------------|" << endl;
  ss << "| " << CLR_INI << "1" << CLR_DEF << "   " << CLR_TXT << "COBOL Squadron Bomber" << CLR_DEF << "  " << CLR_RES << "RES" << CLR_DEF << " |" << endl;
  ss << "|                                |" << endl;
  ss << "| "<<CLR_FLV<<"Whether the ordnance silos"<<CLR_DEF<<" "<<CLR_TXT<<"<@>"<<CLR_DEF<<" |" << endl;
  ss << "| "<<CLR_FLV<<"of their StarFortresses"<<CLR_DEF<<"        |" << endl;
  ss << "| "<<CLR_FLV<<"are loaded with proton"<<CLR_DEF<<"     "<<CLR_TXT<<"[+]"<<CLR_DEF<<" |" << endl;
  ss << "| "<<CLR_FLV<<"bombs or relief supplies,"<<CLR_DEF<<"      |" << endl;
  ss << "| "<<CLR_FLV<<"the heroic crews of Cobalt"<<CLR_DEF<<" "<<CLR_TXT<<"(^)"<<CLR_DEF<<" |" << endl;
  ss << "| "<<CLR_FLV<<"Squadron dedicate their"<<CLR_DEF<<"        |" << endl;
  ss << "| "<<CLR_FLV<<"lives to making a"<<CLR_DEF<<"          "<<CLR_TXT<<"(↗)"<<CLR_DEF" |" << endl;
  ss << "| "<<CLR_FLV<<"difference in the galaxy."<<CLR_DEF<<"      |" << endl;
  ss << "|                                |" << endl;
  ss << "|  "<<CLR_ATK<<"\\ /"<<CLR_DEF<<"  "<<CLR_ATK<<"<o>"<<CLR_DEF<<"  "<<CLR_AGI<<"-->"<<CLR_DEF<<"  "<<CLR_HUL<<"[ ]"<<CLR_DEF<<"  "<<CLR_SHD<<"( )"<<CLR_DEF<<"       |" << endl;
  ss << "|   "<<CLR_ATK<<"3"<<CLR_DEF<<"    "<<CLR_ATK<<"2"<<CLR_DEF<<"    "<<CLR_AGI<<"1"<<CLR_DEF<<"    "<<CLR_HUL<<"9"<<CLR_DEF<<"    "<<CLR_SHD<<"3"<<CLR_DEF<<"        |" << endl;
  ss << "|      MG-100 StarFortress       |" << endl;
  ss << "+--------------------------------+" << endl;
  return ss;
}

std::stringstream MajVonNeumann() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|            "<<RED<<"____"<<CLR_DEF<<"                |" << endl;
  ss << "|         "<<RED<<"__/ / /``--,,"<<CLR_DEF<<"          |" << endl;
  ss << "|        "<<RED<<"/___/ /  _____``--"<<CLR_DEF<<"      |" << endl;
  ss << "|       "<<RED<<"| ___  "<<BRBLACK<<"{}>"<<RED<<"_____"<<CLR_DEF<<"          |" << endl;
  ss << "|        "<<RED<<"\\__ \\ \\       ,,--"<<CLR_DEF<<"      |" << endl;
  ss << "|           "<<RED<<"\\_\\_\\,,--``"<<CLR_DEF<<"          |" << endl;
  ss << "|--------------------------------|" << endl;
  ss << "| " << CLR_INI << "6" << CLR_DEF << "     " << CLR_TXT << "*Maj von Neumann" << CLR_DEF << "     " << CLR_FIR << "F O" << CLR_DEF << " |" << endl;
  ss << "|           "<<CLR_FLV<<"Red Baron"<<CLR_DEF<<"            |" << endl;
  ss << "| During the System Phase,   "<<CLR_TXT<<"<@>"<<CLR_DEF<<" |" << endl;
  ss << "| you may choose 1 enemy         |" << endl;
  ss << "| ship in your BULLSEYE.     "<<CLR_TXT<<"~~>"<<CLR_DEF<<" |" << endl;
  ss << "| That ship gains 1 deplete      |" << endl;
  ss << "| or strain token of your    "<<CLR_TXT<<"[+]"<<CLR_DEF<<" |" << endl;
  ss << "| choice.                        |" << endl;
  ss << "|                            "<<CLR_TXT<<"_o_"<<CLR_DEF<<" |" << endl;
  ss << "|    "<<CLR_FLV<<"Fine-Tuned Thrusters"<<CLR_DEF<<"        |" << endl;
  ss << "|                            "<<CLR_TXT<<"\\|/"<<CLR_DEF<<" |" << endl;
  ss << "|  "<<CLR_ATK<<"\\ /"<<CLR_DEF<<"   "<<CLR_AGI<<"-->"<<CLR_DEF<<"   "<<CLR_HUL<<"[ ]"<<CLR_DEF<<"   "<<CLR_SHD<<"( )"<<CLR_DEF<<"         |" << endl;
  ss << "|   "<<CLR_ATK<<"3"<<CLR_DEF<<"     "<<CLR_AGI<<"3"<<CLR_DEF<<"     "<<CLR_HUL<<"2"<<CLR_DEF<<"     "<<CLR_SHD<<"2"<<CLR_DEF<<"          |" << endl;
  ss << "|       TIE/ba Interceptor       |" << endl;
  ss << "+--------------------------------+" << endl;
  return ss;
}

std::stringstream MaceWindows() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|"<<CLR_BSP<<"               A                "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_BSP<<"              /|\\               "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_BSP<<"             //|\\\\              "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_BSP<<"            // o \\\\             "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_BSP<<"            `T(O)T`             "<<CLR_DEF<<"|" << endl;
  ss << "|"<<CLR_BSP<<"               Y                "<<CLR_DEF<<"|" << endl;
  ss << "|--------------------------------|" << endl;
  ss << "| " << CLR_INI << "4" << CLR_DEF << "      " << CLR_TXT << "*Mace Windows" << CLR_DEF << "       " << CLR_REP << "REP" << CLR_DEF << " |" << endl;
  ss << "|      "<<CLR_FLV<<"Harsh Traditionalist"<<CLR_DEF<<"      |" << endl;
  ss << "| After you fully execute a  "<<CLR_TXT<<"<@>"<<CLR_DEF<<" |" << endl;
  ss << "| red maneuver, recover 1        |" << endl;
  ss << "| FORCE.                     "<<CLR_FRC<<"~~>"<<CLR_DEF<<" |" << endl;
  ss << "|    "<<CLR_FLV<<"Fine-tuned Controls:"<<CLR_DEF<<"        |" << endl;
  ss << "| After you fully execute a  "<<CLR_TXT<<"[+]"<<CLR_DEF<<" |" << endl;
  ss << "| maneuver, you may spend 1      |" << endl;
  ss << "| FORCE to perform a \\|/     "<<CLR_TXT<<"_o_"<<CLR_DEF" |" << endl;
  ss << "| or _o_ action.                 |" << endl;
  ss << "|                            "<<CLR_TXT<<"\\|/"<<CLR_DEF<<" |" << endl;
  ss << "|  "<<CLR_ATK<<"\\ /"<<CLR_DEF<<"  "<<CLR_AGI<<"-->"<<CLR_DEF<<"  "<<CLR_HUL<<"[ ]"<<CLR_DEF<<"  "<<CLR_SHD<<"( )"<<CLR_DEF<<"  "<<CLR_FRC<<"{ }"<<CLR_DEF<<"       |" << endl;
  ss << "|   "<<CLR_ATK<<"2"<<CLR_DEF<<"    "<<CLR_AGI<<"3"<<CLR_DEF<<"    "<<CLR_HUL<<"3"<<CLR_DEF<<"    "<<CLR_SHD<<"1"<<CLR_DEF<<"    "<<CLR_FRC<<"3^"<<CLR_DEF<<"       |" << endl;
  ss << "|      Delta-7 Aethersprite      |" << endl;
  ss << "+--------------------------------+" << endl;
  return ss;
}

std::stringstream DBS404() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+--------------------------------+" << endl;
  ss << "|                                |" << endl;
  ss << "|                                |" << endl;
  ss << "|        "<<CLR_404<<"IMAGE NOT FOUND!"<<CLR_DEF<<"        |" << endl;
  ss << "|                                |" << endl;
  ss << "|                                |" << endl;
  ss << "|                                |" << endl;
  ss << "|--------------------------------|" << endl;
  ss << "| " << CLR_INI << "4" << CLR_DEF << "         " << CLR_TXT << "*DBS-404" << CLR_DEF << "         " << CLR_SEP << "SEP" << CLR_DEF << " |" << endl;
  ss << "|  "<<CLR_FLV<<"Preserve Protocol Not Found"<<CLR_DEF<<"   |" << endl;
  ss << "| You can perform primary  "<<CLR_TXT<<"[@]"<<CLR_DEF<<"   |" << endl;
  ss << "| attacks at range 0.            |" << endl;
  ss << "| While you perform an     "<<CLR_TXT<<"[+]"<<CLR_DEF<<"   |" << endl;
  ss << "| attack at range 0-1,           |" << endl;
  ss << "| you must roll 1       "<<CLR_TXT<<"_o_"<<CLR_DEF<<" > "<<CLR_DIF<<"[+]"<<CLR_DEF<<"|" << endl;
  ss << "| additional die.                |" << endl;
  ss << "| After the attack hits,   "<<CLR_DIF<<"(↗)"<<CLR_DEF<<"   |" << endl;
  ss << "| suffer 1 CRIT damage.          |" << endl;
  ss << "|     "<<CLR_FLV<<"Networked Calculations"<<CLR_DEF<<"     |" << endl;
  ss << "|  "<<CLR_ATK<<"\\ /"<<CLR_DEF<<"        "<<CLR_AGI<<"-->"<<CLR_DEF<<"        "<<CLR_HUL<<"[ ]"<<CLR_DEF<<"     |" << endl;
  ss << "|   "<<CLR_ATK<<"2"<<CLR_DEF<<"          "<<CLR_AGI<<"2"<<CLR_DEF<<"          "<<CLR_HUL<<"5"<<CLR_DEF<<"      |" << endl;
  ss << "|    Hyena-class Droid Bomber    |" << endl;
  ss << "+--------------------------------+" << endl;
  return ss;
}

std::stringstream BafflingElectronics() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+-----------------+----------------------------+" << endl;
  ss << "|                 |      "<<BRBLACK<<"__________________"<<CLR_DEF<<"    |" << endl;
  ss << "|                 |     "<<BRBLACK<<"/_"<<CLR_DEF<<"   ,---.        "<<BRBLACK<<"/\\"<<CLR_DEF<<"   |" << endl;
  ss << "| [MODIFICATION]  |    "<<BRBLACK<<"_ /"<<CLR_DEF<<"   \\_   `-- o  "<<BRBLACK<<"/ /"<<CLR_DEF<<"\\  |" << endl;
  ss << "|                 |   "<<BRBLACK<<"/_________________/ /"<<CLR_DEF<<"    |" << endl;
  ss << "|                 |   "<<BRBLACK<<"\\_________________\\/"<<CLR_DEF<<"     |" << endl;
  ss << "|                 |    \\  \\  \\  \\  \\  \\  \\     |" << endl;
  ss << "|                 +----------------------------+" << endl;
  ss << "|                 |    "<<CLR_TXT<<"Baffling Electronics"<<CLR_DEF<<"    |" << endl;
  ss << "|                 |                            |" << endl;
  ss << "|                 | During the End Phase, you  |" << endl;
  ss << "|                 | may suffer 1 HIT damage to |" << endl;
  ss << "|                 | remove 1 red token.        |" << endl;
  ss << "|                 |                            |" << endl;
  ss << "|                 |                            |" << endl;
  ss << "|                 |                            |" << endl;
  ss << "+-----------------+----------------------------+" << endl;
  return ss;
}

std::stringstream ForkBomb() {
  std::stringstream ss;
  ss << CLR_DEF;
  ss << "+-----------------+----------------------------+" << endl;
  ss << "|                 |                            |" << endl;
  ss << "|                 |                            |" << endl;
  ss << "|    [PAYLOAD]    |        "<<BRGREEN<<":(){ :|:& };:"<<CLR_DEF<<"       |" << endl;
  ss << "|                 |                            |" << endl;
  ss << "| [MODIFICATION]  |                            |" << endl;
  ss << "|                 |                            |" << endl;
  ss << "|                 +----------------------------+" << endl;
  ss << "|                 |         "<<CLR_TXT<<"*Fork Bomb"<<CLR_DEF<<"   "<<CLR_CHG<<"(Z) 1"<<CLR_DEF<<" |" << endl;
  ss << "|                 | During the System Phase,   |" << endl;
  ss << "|                 | you may spend 1 CHARGE to  |" << endl;
  ss << "|                 | drop an Electro-Proton Bomb|" << endl;
  ss << "|                 | with the [1↑] template.    |" << endl;
  ss << "|                 | Then place 1 fuse marker   |" << endl;
  ss << "|                 | on that device.  This cards|" << endl;
  ss << "|       "<<CLR_BOW<<"(↗)"<<CLR_DEF<<"       | CHARGE cannot be recovered.|" << endl;
  ss << "+-----------------+----------------------------+" << endl;
  return ss;
}

}
