#pragma once
#include "colors.h"
#include <sstream>


namespace tech2020 {
  std::stringstream Back();
  std::stringstream BashRendar();
  std::stringstream Echo();
  std::stringstream SataPortHunter();
  std::stringstream CobolSqBomber();
  std::stringstream MajVonNeumann();
  std::stringstream MaceWindows();
  std::stringstream DBS404();
  std::stringstream BafflingElectronics();
  std::stringstream ForkBomb();
}
